int pin_pot1 = A0;
int pin_pot2 = A1;
//--------------------------------------
int pin_led = 11;
//--------------------------------------

#include <Fuzzy.h>
Fuzzy *fuzzy = new Fuzzy();

// FuzzyInput analog1
FuzzySet *rendah1          = new FuzzySet(0, 0, 250, 380);
FuzzySet *normal1          = new FuzzySet(200, 500, 500, 800);
FuzzySet *tinggi1          = new FuzzySet(600, 800, 1023, 1023);

// FuzzyInput analog2
FuzzySet *rendah2          = new FuzzySet(0, 0, 250, 380);
FuzzySet *normal2          = new FuzzySet(200, 500, 500, 800);
FuzzySet *tinggi2          = new FuzzySet(600, 800, 1023, 1023);

// FuzzyOutput
FuzzySet *redup            = new FuzzySet(0, 0, 60, 100);
FuzzySet *setengah         = new FuzzySet(70, 140, 140, 180);
FuzzySet *terang           = new FuzzySet(130, 180, 255, 255);


void setup()
{
  Serial.begin(115200);
  pinMode(pin_led , OUTPUT);

  //--------------------------------------

  // FuzzyInput
  FuzzyInput *analog1 = new FuzzyInput(1);

  analog1->addFuzzySet(rendah1);
  analog1->addFuzzySet(normal1);
  analog1->addFuzzySet(tinggi1);
  fuzzy->addFuzzyInput(analog1);



  // FuzzyInput
  FuzzyInput *analog2 = new FuzzyInput(2);

  analog2->addFuzzySet(rendah2);
  analog2->addFuzzySet(normal2);
  analog2->addFuzzySet(tinggi2);
  fuzzy->addFuzzyInput(analog2);



  // FuzzyOutput
  FuzzyOutput *led = new FuzzyOutput(1);

  led->addFuzzySet(redup);
  led->addFuzzySet(setengah);
  led->addFuzzySet(terang);
  fuzzy->addFuzzyOutput(led);
  //--------------------------------------






  // Building FuzzyRule////////////////////////////////////////////////////////////////// 1
  FuzzyRuleAntecedent *rendah1_rendah2 = new FuzzyRuleAntecedent();
  rendah1_rendah2->joinWithAND(rendah1, rendah2);

  FuzzyRuleConsequent *led_redup1 = new FuzzyRuleConsequent();
  led_redup1->addOutput(redup);

  FuzzyRule *fuzzyRule1 = new FuzzyRule(1, rendah1_rendah2, led_redup1);
  fuzzy->addFuzzyRule(fuzzyRule1);

  // Building FuzzyRule////////////////////////////////////////////////////////////////// 2
  FuzzyRuleAntecedent *rendah1_normal2 = new FuzzyRuleAntecedent();
  rendah1_normal2->joinWithAND(rendah1, normal2);

  FuzzyRuleConsequent *led_redup2 = new FuzzyRuleConsequent();
  led_redup2->addOutput(redup);

  FuzzyRule *fuzzyRule2 = new FuzzyRule(2, rendah1_normal2, led_redup2);
  fuzzy->addFuzzyRule(fuzzyRule2);

  // Building FuzzyRule////////////////////////////////////////////////////////////////// 3
  FuzzyRuleAntecedent *rendah1_tinggi2 = new FuzzyRuleAntecedent();
  rendah1_tinggi2->joinWithAND(rendah1, tinggi2);

  FuzzyRuleConsequent *led_setengah3 = new FuzzyRuleConsequent();
  led_setengah3->addOutput(setengah);

  FuzzyRule *fuzzyRule3 = new FuzzyRule(3, rendah1_tinggi2, led_setengah3);
  fuzzy->addFuzzyRule(fuzzyRule3);

  // Building FuzzyRule////////////////////////////////////////////////////////////////// 4
  FuzzyRuleAntecedent *normal1_rendah2 = new FuzzyRuleAntecedent();
  normal1_rendah2->joinWithAND(normal1, rendah2);

  FuzzyRuleConsequent *led_redup4 = new FuzzyRuleConsequent();
  led_redup4->addOutput(redup);

  FuzzyRule *fuzzyRule4 = new FuzzyRule(4, normal1_rendah2, led_redup4);
  fuzzy->addFuzzyRule(fuzzyRule4);

  // Building FuzzyRule////////////////////////////////////////////////////////////////// 5
  FuzzyRuleAntecedent *normal1_normal2 = new FuzzyRuleAntecedent();
  normal1_normal2->joinWithAND(normal1, normal2);

  FuzzyRuleConsequent *led_setengah5 = new FuzzyRuleConsequent();
  led_setengah5->addOutput(setengah);

  FuzzyRule *fuzzyRule5 = new FuzzyRule(5, normal1_normal2, led_setengah5);
  fuzzy->addFuzzyRule(fuzzyRule5);

  // Building FuzzyRule////////////////////////////////////////////////////////////////// 6
  FuzzyRuleAntecedent *normal1_tinggi2 = new FuzzyRuleAntecedent();
  normal1_tinggi2->joinWithAND(normal1, tinggi2);

  FuzzyRuleConsequent *led_terang6 = new FuzzyRuleConsequent();
  led_terang6->addOutput(terang);

  FuzzyRule *fuzzyRule6 = new FuzzyRule(6, normal1_tinggi2, led_terang6);
  fuzzy->addFuzzyRule(fuzzyRule6);

  // Building FuzzyRule////////////////////////////////////////////////////////////////// 7
  FuzzyRuleAntecedent *tinggi1_rendah2 = new FuzzyRuleAntecedent();
  tinggi1_rendah2->joinWithAND(tinggi1, rendah2);

  FuzzyRuleConsequent *led_setengah7 = new FuzzyRuleConsequent();
  led_setengah7->addOutput(setengah);

  FuzzyRule *fuzzyRule7 = new FuzzyRule(7, tinggi1_rendah2, led_setengah7);
  fuzzy->addFuzzyRule(fuzzyRule7);

  // Building FuzzyRule////////////////////////////////////////////////////////////////// 8
  FuzzyRuleAntecedent *tinggi1_normal2 = new FuzzyRuleAntecedent();
  tinggi1_normal2->joinWithAND(tinggi1, normal2);

  FuzzyRuleConsequent *led_terang8 = new FuzzyRuleConsequent();
  led_terang8->addOutput(terang);

  FuzzyRule *fuzzyRule8 = new FuzzyRule(8, tinggi1_normal2, led_terang8);
  fuzzy->addFuzzyRule(fuzzyRule8);

  // Building FuzzyRule////////////////////////////////////////////////////////////////// 9
  FuzzyRuleAntecedent *tinggi1_tinggi2 = new FuzzyRuleAntecedent();
  tinggi1_tinggi2->joinWithAND(tinggi1, tinggi2);

  FuzzyRuleConsequent *led_terang9 = new FuzzyRuleConsequent();
  led_terang9->addOutput(terang);

  FuzzyRule *fuzzyRule9 = new FuzzyRule(9, tinggi1_tinggi2, led_terang9);
  fuzzy->addFuzzyRule(fuzzyRule9);

}



void loop()
{
  int in_analog1 = analogRead(pin_pot1);
  int in_analog2 = analogRead(pin_pot2);

//  int in_analog1 = random(0, 1023);
//  int in_analog2 = random(0, 1023);
  
  //--------------------------------------

  fuzzy->setInput(1, in_analog1);
  fuzzy->setInput(2, in_analog2);
  fuzzy->fuzzify();


  int out_led = fuzzy->defuzzify(1);
  analogWrite(pin_led , out_led);

  //--------------------------------------
  
  Serial.print("pot1    ");
  Serial.print(in_analog1);  Serial.println("  bit");
  Serial.print("pot2    ");
  Serial.print(in_analog2);  Serial.println("  bit");

  //---------------------

  Serial.print(in_analog1);   Serial.print(" ");
  Serial.println(in_analog2);
  Serial.println(" ");

  Serial.println("Result: ");
  Serial.print("       led: ");
  Serial.println(out_led);
  Serial.println("");
  Serial.println("");
}
