#include <Fuzzy.h>

// For scope, instantiate all objects you will need to access in loop()
// It may be just one Fuzzy, but for demonstration, this sample will print
// all FuzzySet pertinence

// Fuzzy
Fuzzy *fuzzy = new Fuzzy();

// FuzzyInput cuaca
FuzzySet *hujan = new FuzzySet(-400, -300, 300, 400);
FuzzySet *gerimis = new FuzzySet(300, 400, 800, 900);
FuzzySet *cerah = new FuzzySet(800, 900, 1200, 1600);

// FuzzyInput cahaya
FuzzySet *gelap = new FuzzySet(-250, -200, 200, 250);
FuzzySet *berawan = new FuzzySet(200, 250, 350, 400);
FuzzySet *terang = new FuzzySet(350, 400, 550, 600);



// FuzzyOutput kecepatan
FuzzySet *cepatKedalam = new FuzzySet(-150,-100 , -80, -50);
FuzzySet *lambatKedalam = new FuzzySet(-80, -50, -20, 10);
FuzzySet *lambatKeluar = new FuzzySet(-10, 20, 50, 80);
FuzzySet *cepatKeluar = new FuzzySet (50, 80, 100,150);


void setup()
{
  // Set the Serial output
  Serial.begin(9600);
  // Set a random seed
  randomSeed(analogRead(0));

  // Every setup must occur in the function setup()

  // FuzzyInput
  FuzzyInput *cuaca = new FuzzyInput(1);

  cuaca->addFuzzySet(hujan);
  cuaca->addFuzzySet(gerimis);
  cuaca->addFuzzySet(cerah);
  fuzzy->addFuzzyInput(cuaca);

  // FuzzyInput
  FuzzyInput *cahaya = new FuzzyInput(2);

  cahaya->addFuzzySet(gelap);
  cahaya->addFuzzySet(berawan);
  cahaya->addFuzzySet(terang);
  fuzzy->addFuzzyInput(cahaya);

  // FuzzyOutput
  FuzzyOutput *kecepatan = new FuzzyOutput(1);

  kecepatan->addFuzzySet(cepatKedalam);
  kecepatan->addFuzzySet(lambatKedalam);
  kecepatan->addFuzzySet(lambatKeluar);
  kecepatan->addFuzzySet(cepatKeluar);
  fuzzy->addFuzzyOutput(kecepatan);


  // Building FuzzyRule 1
  FuzzyRuleAntecedent *hujan1_gelap1 = new FuzzyRuleAntecedent();
  hujan1_gelap1->joinWithAND(hujan, gelap);

  FuzzyRuleConsequent *kecepatan1 = new FuzzyRuleConsequent();
  kecepatan1->addOutput(cepatKedalam);

  FuzzyRule *fuzzyRule1 = new FuzzyRule(1, hujan1_gelap1, kecepatan1);
  fuzzy->addFuzzyRule(fuzzyRule1);

  // 2
  FuzzyRuleAntecedent *hujan_berawan = new FuzzyRuleAntecedent();
  hujan_berawan->joinWithAND(hujan, berawan);

  FuzzyRuleConsequent *kecepatan2 = new FuzzyRuleConsequent();
  kecepatan2->addOutput(cepatKedalam);

  FuzzyRule *fuzzyRule2 = new FuzzyRule(2, hujan_berawan, kecepatan2);
  fuzzy->addFuzzyRule(fuzzyRule2);

  //3
  FuzzyRuleAntecedent *hujan_terang = new FuzzyRuleAntecedent();
  hujan_terang->joinWithAND(hujan, terang);

  FuzzyRuleConsequent *kecepatan3 = new FuzzyRuleConsequent();
  kecepatan3->addOutput(cepatKedalam);

  FuzzyRule *fuzzyRule3 = new FuzzyRule(3, hujan_terang, kecepatan3);
  fuzzy->addFuzzyRule(fuzzyRule3);

  //4

  FuzzyRuleAntecedent *gerimis_gelap = new FuzzyRuleAntecedent();
  gerimis_gelap->joinWithAND(gerimis, gelap);

  FuzzyRuleConsequent *kecepatan4 = new FuzzyRuleConsequent();
  kecepatan4->addOutput(lambatKedalam);

  FuzzyRule *fuzzyRule4 = new FuzzyRule(4, gerimis_gelap, kecepatan4);
  fuzzy->addFuzzyRule(fuzzyRule4);

  //5
  FuzzyRuleAntecedent *gerimis_berawan = new FuzzyRuleAntecedent();
  gerimis_berawan->joinWithAND(gerimis, berawan);

  FuzzyRuleConsequent *kecepatan5 = new FuzzyRuleConsequent();
  kecepatan5->addOutput(lambatKedalam);

  FuzzyRule *fuzzyRule5 = new FuzzyRule(5, gerimis_berawan, kecepatan5);
  fuzzy->addFuzzyRule(fuzzyRule5);

  //6
  FuzzyRuleAntecedent *gerimis_terang = new FuzzyRuleAntecedent();
  gerimis_terang->joinWithAND(gerimis, terang);

  FuzzyRuleConsequent *kecepatan6 = new FuzzyRuleConsequent();
  kecepatan6->addOutput(lambatKedalam);

  FuzzyRule *fuzzyRule6 = new FuzzyRule(6, gerimis_terang, kecepatan6);
  fuzzy->addFuzzyRule(fuzzyRule6);

  // 7 
  FuzzyRuleAntecedent *cerah_gelap = new FuzzyRuleAntecedent();
  cerah_gelap->joinWithAND(cerah, gelap);

  FuzzyRuleConsequent *kecepatan7 = new FuzzyRuleConsequent();
  kecepatan7->addOutput(lambatKeluar);

  FuzzyRule *fuzzyRule7 = new FuzzyRule(7, cerah_gelap, kecepatan7);
  fuzzy->addFuzzyRule(fuzzyRule7);

  //8 
  FuzzyRuleAntecedent *cerah_berawan = new FuzzyRuleAntecedent();
  cerah_berawan->joinWithAND(cerah, berawan);

  FuzzyRuleConsequent *kecepatan8 = new FuzzyRuleConsequent();
  kecepatan8->addOutput(lambatKeluar);

  FuzzyRule *fuzzyRule8 = new FuzzyRule(8, cerah_berawan, kecepatan8);
  fuzzy->addFuzzyRule(fuzzyRule8);

  // 9 

  FuzzyRuleAntecedent *cerah_terang = new FuzzyRuleAntecedent();
  cerah_terang->joinWithAND(cerah, terang);

  FuzzyRuleConsequent *kecepatan9 = new FuzzyRuleConsequent();
  kecepatan9->addOutput(cepatKeluar);

  FuzzyRule *fuzzyRule9 = new FuzzyRule(9, cerah_terang, kecepatan9);
  fuzzy->addFuzzyRule(fuzzyRule9);

}

void loop()
{
  // get random entrances
  int input1 = random(0, 1023);
  int input2 = random(0, 500);
  //int input3 = random(-30, 30);

  Serial.println("\n\n\nEntrance: ");
  Serial.print("\t\t\tCuaca: ");
  Serial.print(input1);
  Serial.print(", Cahaya: ");
  Serial.print(input2);
  
  fuzzy->setInput(1, input1);
  fuzzy->setInput(2, input2);
  //fuzzy->setInput(3, input3);

  fuzzy->fuzzify();

  Serial.println("Input: ");
  Serial.print("\thujan : hujan-> ");
  Serial.print(hujan->getPertinence());
  Serial.print(", gerimis-> ");
  Serial.print(gerimis->getPertinence());
  Serial.print(", Cerah-> ");
  Serial.println(cerah->getPertinence());

  Serial.print("\tgelap: gelap-> ");
  Serial.print(gelap->getPertinence());
  Serial.print(",  Berawan-> ");
  Serial.print(berawan->getPertinence());
  Serial.print(",  Terang-> ");
  Serial.print(terang->getPertinence());

//  Serial.print("\tTemperature: Cold-> ");
//  Serial.print(cold->getPertinence());
//  Serial.print(", Good-> ");
//  Serial.print(good->getPertinence());
//  Serial.print(", Hot-> ");
//  Serial.println(hot->getPertinence());

  float output1 = fuzzy->defuzzify(1);
 // float output2 = fuzzy->defuzzify(2);

  Serial.println("Output: ");
  Serial.print("\tkecepetan: kedalam-> ");
  Serial.print(cepatKedalam->getPertinence());
  Serial.print(", Kecepatan-> ");
  Serial.print(lambatKedalam->getPertinence());
  Serial.print(", kecepatan-> ");
  Serial.println(lambatKeluar->getPertinence());
  Serial.print(", kecepatan-> ");
  Serial.println(cepatKeluar->getPertinence());
  

//  Serial.print("\tSpeed: Stoped-> ");
//  Serial.print(stopedOutput->getPertinence());
//  Serial.print(",  Slow-> ");
//  Serial.print(slowOutput->getPertinence());
//  Serial.print(",  Normal-> ");
//  Serial.print(normalOutput->getPertinence());
//  Serial.print(",  Quick-> ");
//  Serial.println(quickOutput->getPertinence());

  Serial.println("Result: ");
  Serial.print("\t\t\tKecepatan: ");
  Serial.print(output1);
//  Serial.print(", and Speed: ");
//  Serial.println(output2);

  // wait 12 seconds
  delay(12000);
}
