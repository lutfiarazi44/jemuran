#include <Fuzzy.h>

// For scope, instantiate all objects you will need to access in loop()
// It may be just one Fuzzy, but for demonstration, this sample will print
// all FuzzySet pertinence

// Fuzzy
Fuzzy *fuzzy = new Fuzzy();

// FuzzyInput cahaya
FuzzySet *redup = new FuzzySet(0, 0, 15, 50);
FuzzySet *terang = new FuzzySet(30, 50, 70, 80);
FuzzySet *sangatTerang = new FuzzySet(70, 80, 100, 100);

// FuzzyInput kelembaban
FuzzySet *kering = new FuzzySet(0, 0, 45, 60);
FuzzySet *normal = new FuzzySet(50, 60, 70, 90);
FuzzySet *lembab = new FuzzySet(80, 90, 100, 100);

// FuzzyInput suhu
FuzzySet *dingin = new FuzzySet(0, 0, 20, 25);
FuzzySet *hangat = new FuzzySet(20, 25, 32, 35);
FuzzySet *panas = new FuzzySet(32, 35, 50, 50);


// FuzzyOutput cuaca
FuzzySet *mendung = new FuzzySet(0, 0, 2, 6);
FuzzySet *cerah = new FuzzySet(4, 8, 10, 10);

int LDR_Pin = A0; // pin a1 analog
int pinSensor = A1;
void setup()
{
  // Set the Serial output
  Serial.begin(9600);
  // Set a random seed
  randomSeed(analogRead(0));

  // Every setup must occur in the function setup()

  // FuzzyInput
  FuzzyInput *cahaya = new FuzzyInput(1);

  cahaya->addFuzzySet(redup);
  cahaya->addFuzzySet(terang);
  cahaya->addFuzzySet(sangatTerang);
  fuzzy->addFuzzyInput(cahaya);

  // FuzzyInput
  FuzzyInput *kelembaban = new FuzzyInput(2);

  kelembaban->addFuzzySet(kering);
  kelembaban->addFuzzySet(normal);
  kelembaban->addFuzzySet(lembab);
  fuzzy->addFuzzyInput(kelembaban);

  //fuzzy input
  FuzzyInput *suhu = new FuzzyInput(3);

  suhu->addFuzzySet(dingin);
  suhu->addFuzzySet(hangat);
  suhu->addFuzzySet(panas);
  fuzzy->addFuzzyInput(suhu);

  // FuzzyOutput
  FuzzyOutput *cuaca = new FuzzyOutput(1);

  cuaca->addFuzzySet(mendung);
  cuaca->addFuzzySet(cerah);
  fuzzy->addFuzzyOutput(cuaca);

//==============================================================
//    // Building FuzzyRule 1
//  FuzzyRuleAntecedent *dingin_kering1 = new FuzzyRuleAntecedent();
//  dingin_kering1->joinWithAND(dingin, kering);
//  
//  FuzzyRuleAntecedent *redup1 = new FuzzyRuleAntecedent();
//  redup1->joinSingle(redup);
//
//  FuzzyRuleAntecedent *dingin_kering_redup_1 = new FuzzyRuleAntecedent();
//  dingin_kering_redup_1->joinWithAND(dingin_kering1, redup1);
//  
//  FuzzyRuleConsequent *cuaca1 = new FuzzyRuleConsequent();
//  cuaca1->addOutput(mendung);
//
//  FuzzyRule *fuzzyRule1 = new FuzzyRule(1, dingin_kering_redup_1, cuaca1);
//  fuzzy->addFuzzyRule(fuzzyRule1);
//
//  // 2
//  FuzzyRuleAntecedent *dingin_kering2 = new FuzzyRuleAntecedent();
//  dingin_kering2->joinWithAND(dingin, kering);
//
//  FuzzyRuleAntecedent *terang2 = new FuzzyRuleAntecedent();
//  terang2->joinSingle(terang);
//
//  FuzzyRuleAntecedent *dingin_kering_terang_2 = new FuzzyRuleAntecedent();
//  dingin_kering_terang_2->joinWithAND(dingin_kering2, terang2);
//
//  FuzzyRuleConsequent *cuaca2 = new FuzzyRuleConsequent();
//  cuaca2->addOutput(cerah);
//
//  FuzzyRule *fuzzyRule2 = new FuzzyRule(2, dingin_kering_terang_2, cuaca2);
//  fuzzy->addFuzzyRule(fuzzyRule2);
//==============================================================

//  // Building FuzzyRule 1
//  FuzzyRuleAntecedent *redup_kering1 = new FuzzyRuleAntecedent();
//  redup_kering1->joinWithAND(redup, kering);
//  
//  FuzzyRuleAntecedent *dingin1 = new FuzzyRuleAntecedent();
//  dingin1->joinSingle(dingin);
//
//  FuzzyRuleAntecedent *redup_kering_dingin = new FuzzyRuleAntecedent();
//  redup_kering_dingin->joinWithAND(redup_kering1, dingin1);
//  
//  FuzzyRuleConsequent *cuaca1 = new FuzzyRuleConsequent();
//  cuaca1->addOutput(mendung);
//
//  FuzzyRule *fuzzyRule1 = new FuzzyRule(1, redup_kering_dingin, cuaca1);
//  fuzzy->addFuzzyRule(fuzzyRule1);
//
//  // 2
//  FuzzyRuleAntecedent *redup_kering2 = new FuzzyRuleAntecedent();
//  redup_kering2->joinWithAND(redup, kering);
//
//  FuzzyRuleAntecedent *hangat2 = new FuzzyRuleAntecedent();
//  hangat2->joinSingle(hangat);
//
//  FuzzyRuleAntecedent *redup_kering_hangat = new FuzzyRuleAntecedent();
//  redup_kering_hangat->joinWithAND(redup_kering2, hangat2);
//
//  FuzzyRuleConsequent *cuaca2 = new FuzzyRuleConsequent();
//  cuaca2->addOutput(cerah);
//
//  FuzzyRule *fuzzyRule2 = new FuzzyRule(2, redup_kering_hangat, cuaca2);
//  fuzzy->addFuzzyRule(fuzzyRule2);
//
//  //3
//  FuzzyRuleAntecedent *redup_kering3 = new FuzzyRuleAntecedent();
//  redup_kering3->joinWithAND(redup, kering);
//
//  FuzzyRuleAntecedent *panas3 = new FuzzyRuleAntecedent();
//  panas3->joinSingle(panas);
//
//  FuzzyRuleAntecedent *redup_kering_panas = new FuzzyRuleAntecedent();
//  redup_kering_panas->joinWithAND(redup_kering3, panas3);
//
//  FuzzyRuleConsequent *cuaca3 = new FuzzyRuleConsequent();
//  cuaca3->addOutput(cerah);
//
//  FuzzyRule *fuzzyRule3 = new FuzzyRule(3, redup_kering_panas, cuaca3);
//  fuzzy->addFuzzyRule(fuzzyRule3);
//
//  //4
//
//  FuzzyRuleAntecedent *redup_normal4 = new FuzzyRuleAntecedent();
//  redup_normal4->joinWithAND(redup, normal);
//
//  FuzzyRuleAntecedent *dingin4 = new FuzzyRuleAntecedent();
//  dingin4->joinSingle(dingin);
//
//  FuzzyRuleAntecedent *redup_normal_dingin = new FuzzyRuleAntecedent();
//  redup_normal_dingin->joinWithAND(redup_normal4, dingin4);
//
//  FuzzyRuleConsequent *cuaca4 = new FuzzyRuleConsequent();
//  cuaca4->addOutput(mendung);
//
//  FuzzyRule *fuzzyRule4 = new FuzzyRule(4, redup_normal_dingin, cuaca4);
//  fuzzy->addFuzzyRule(fuzzyRule4);
//
//  //5
//  FuzzyRuleAntecedent *redup_normal5 = new FuzzyRuleAntecedent();
//  redup_normal5->joinWithAND(redup, normal);
//
//  FuzzyRuleAntecedent *hangat5 = new FuzzyRuleAntecedent();
//  hangat5->joinSingle(hangat);
//
//  FuzzyRuleAntecedent *redup_normal_hangat = new FuzzyRuleAntecedent();
//  redup_normal_hangat->joinWithAND(redup_normal5, hangat5);
//
//  FuzzyRuleConsequent *cuaca5 = new FuzzyRuleConsequent();
//  cuaca5->addOutput(mendung);
//
//  FuzzyRule *fuzzyRule5 = new FuzzyRule(5, redup_normal_hangat, cuaca5);
//  fuzzy->addFuzzyRule(fuzzyRule5);
//
//  //6
//  
//  FuzzyRuleAntecedent *redup_normal6 = new FuzzyRuleAntecedent();
//  redup_normal6->joinWithAND(redup, normal);
//
//  FuzzyRuleAntecedent *panas6 = new FuzzyRuleAntecedent();
//  panas6->joinSingle(panas);
//
//  FuzzyRuleAntecedent *redup_normal_panas = new FuzzyRuleAntecedent();
//  redup_normal_panas->joinWithAND(redup_normal6, panas6);
//
//  FuzzyRuleConsequent *cuaca6 = new FuzzyRuleConsequent();
//  cuaca6->addOutput(cerah);
//
//  FuzzyRule *fuzzyRule6 = new FuzzyRule(6, redup_normal_panas, cuaca6);
//  fuzzy->addFuzzyRule(fuzzyRule6);
//
//  // 7 
//  FuzzyRuleAntecedent *redup_lembab7 = new FuzzyRuleAntecedent();
//  redup_lembab7->joinWithAND(redup, lembab);
//
//  FuzzyRuleAntecedent *dingin7 = new FuzzyRuleAntecedent();
//  dingin7->joinSingle(dingin);
//
//  FuzzyRuleAntecedent *redup_lembab_dingin = new FuzzyRuleAntecedent();
//  redup_lembab_dingin->joinWithAND(redup_lembab7, dingin7);
//
//  FuzzyRuleConsequent *cuaca7 = new FuzzyRuleConsequent();
//  cuaca7->addOutput(mendung);
//
//  FuzzyRule *fuzzyRule7 = new FuzzyRule(7, redup_lembab_dingin, cuaca7);
//  fuzzy->addFuzzyRule(fuzzyRule7);
//
//  //8 
//  FuzzyRuleAntecedent *redup_lembab8 = new FuzzyRuleAntecedent();
//  redup_lembab8->joinWithAND(redup, lembab);
//
//  FuzzyRuleAntecedent *hangat8 = new FuzzyRuleAntecedent();
//  hangat8->joinSingle(hangat);
//  
//  FuzzyRuleAntecedent *redup_lembab_hangat = new FuzzyRuleAntecedent();
//  redup_lembab_hangat->joinWithAND(redup_lembab8, hangat8);
//
//  FuzzyRuleConsequent *cuaca8 = new FuzzyRuleConsequent();
//  cuaca8->addOutput(mendung);
//
//  FuzzyRule *fuzzyRule8 = new FuzzyRule(8, redup_lembab_hangat, cuaca8);
//  fuzzy->addFuzzyRule(fuzzyRule8);
//
//  // 9 
//  FuzzyRuleAntecedent *redup_lembab9 = new FuzzyRuleAntecedent();
//  redup_lembab9->joinWithAND(redup, lembab);
//
//  FuzzyRuleAntecedent *panas9 = new FuzzyRuleAntecedent();
//  panas9->joinSingle(panas);
//
//  FuzzyRuleAntecedent *redup_lembab_panas = new FuzzyRuleAntecedent();
//  redup_lembab_panas->joinWithAND(redup_lembab9, panas9);
//
//  FuzzyRuleConsequent *cuaca9 = new FuzzyRuleConsequent();
//  cuaca9->addOutput(mendung);
//
//  FuzzyRule *fuzzyRule9 = new FuzzyRule(9, redup_lembab_panas, cuaca9);
//  fuzzy->addFuzzyRule(fuzzyRule9);
//
//  //10
//  FuzzyRuleAntecedent *terang_kering10 = new FuzzyRuleAntecedent();
//  terang_kering10->joinWithAND(terang, kering);
//
//  FuzzyRuleAntecedent *dingin10 = new FuzzyRuleAntecedent();
//  dingin10->joinSingle(dingin);
//
//  FuzzyRuleAntecedent *terang_kering_dingin = new FuzzyRuleAntecedent();
//  terang_kering_dingin->joinWithAND(terang_kering10, dingin10);
// 
//  FuzzyRuleConsequent *cuaca10 = new FuzzyRuleConsequent();
//  cuaca10->addOutput(cerah);
//
//  FuzzyRule *fuzzyRule10 = new FuzzyRule(10, terang_kering_dingin, cuaca10);
//  fuzzy->addFuzzyRule(fuzzyRule10);
//
//  //11
//  FuzzyRuleAntecedent *terang_kering11 = new FuzzyRuleAntecedent();
//  terang_kering11->joinWithAND(terang, kering);
//
//  FuzzyRuleAntecedent *hangat11 = new FuzzyRuleAntecedent();
//  hangat11->joinSingle(hangat);
//
//  FuzzyRuleAntecedent *terang_kering_hangat = new FuzzyRuleAntecedent();
//  terang_kering_hangat->joinWithAND(terang_kering11, hangat11);  
//  
//  FuzzyRuleConsequent *cuaca11 = new FuzzyRuleConsequent();
//  cuaca11->addOutput(cerah);
//
//  FuzzyRule *fuzzyRule11 = new FuzzyRule(11, terang_kering_hangat, cuaca11);
//  fuzzy->addFuzzyRule(fuzzyRule11);
//
//  //12
//  FuzzyRuleAntecedent *terang_kering12 = new FuzzyRuleAntecedent();
//  terang_kering12->joinWithAND(terang, kering);
//
//  FuzzyRuleAntecedent *panas12 = new FuzzyRuleAntecedent();
//  panas12->joinSingle(panas);
//
//  FuzzyRuleAntecedent *terang_kering_panas = new FuzzyRuleAntecedent();
//  terang_kering_panas->joinWithAND(terang_kering12, panas12);
//
//  FuzzyRuleConsequent *cuaca12 = new FuzzyRuleConsequent();
//  cuaca12->addOutput(cerah);
//
//  FuzzyRule *fuzzyRule12 = new FuzzyRule(12, terang_kering_panas, cuaca12);
//  fuzzy->addFuzzyRule(fuzzyRule12);

  //13
  FuzzyRuleAntecedent *terang_normal13 = new FuzzyRuleAntecedent();
  terang_normal13->joinWithAND(terang, normal);

  FuzzyRuleAntecedent *dingin13 = new FuzzyRuleAntecedent();
  dingin13->joinSingle(dingin);

  FuzzyRuleAntecedent *terang_normal_dingin = new FuzzyRuleAntecedent();
  terang_normal_dingin->joinWithAND(terang_normal13, dingin13);
  
  FuzzyRuleConsequent *cuaca13 = new FuzzyRuleConsequent();
  cuaca13->addOutput(mendung);

  FuzzyRule *fuzzyRule1 = new FuzzyRule(13,terang_normal_dingin, cuaca13);
  fuzzy->addFuzzyRule(fuzzyRule1);

  //14
  FuzzyRuleAntecedent *terang_normal14 = new FuzzyRuleAntecedent();
  terang_normal14->joinWithAND(terang, normal);

  FuzzyRuleAntecedent *hangat14 = new FuzzyRuleAntecedent();
  hangat14->joinSingle(hangat);

  FuzzyRuleAntecedent *terang_normal_hangat = new FuzzyRuleAntecedent();
  terang_normal_hangat->joinWithAND(terang_normal14, hangat14);

  FuzzyRuleConsequent *cuaca14 = new FuzzyRuleConsequent();
  cuaca14->addOutput(cerah);

  FuzzyRule *fuzzyRule2 = new FuzzyRule(14,terang_normal_hangat, cuaca14);
  fuzzy->addFuzzyRule(fuzzyRule2);
//
//  //15
//  FuzzyRuleAntecedent *terang_normal15 = new FuzzyRuleAntecedent();
//  terang_normal15->joinWithAND(terang, normal);
//
//  FuzzyRuleAntecedent *panas15 = new FuzzyRuleAntecedent();
//  panas15->joinSingle(panas);
//
//  FuzzyRuleAntecedent *terang_normal_panas = new FuzzyRuleAntecedent();
//  terang_normal_panas->joinWithAND(terang_normal15, panas15);
//
//  FuzzyRuleConsequent *cuaca15 = new FuzzyRuleConsequent();
//  cuaca15->addOutput(cerah);
//
//  FuzzyRule *fuzzyRule15 = new FuzzyRule(15,terang_normal_panas, cuaca15);
//  fuzzy->addFuzzyRule(fuzzyRule15);
//
//  //16 
//
//  FuzzyRuleAntecedent *terang_lembab16 = new FuzzyRuleAntecedent();
//  terang_lembab16->joinWithAND(terang, lembab);
//
//  FuzzyRuleAntecedent *dingin16 = new FuzzyRuleAntecedent();
//  dingin16->joinSingle(dingin);
//
//  FuzzyRuleAntecedent *terang_lembab_dingin = new FuzzyRuleAntecedent();
//  terang_lembab_dingin->joinWithAND(terang_lembab16, dingin16);
//
//  FuzzyRuleConsequent *cuaca16 = new FuzzyRuleConsequent();
//  cuaca16->addOutput(mendung);
//
//  FuzzyRule *fuzzyRule16 = new FuzzyRule(16,terang_lembab_dingin, cuaca16);
//  fuzzy->addFuzzyRule(fuzzyRule16);
//
//  //17
//  FuzzyRuleAntecedent *terang_lembab17 = new FuzzyRuleAntecedent();
//  terang_lembab17->joinWithAND(terang, lembab);
//
//  FuzzyRuleAntecedent *hangat17 = new FuzzyRuleAntecedent();
//  hangat17->joinSingle(hangat);
//
//  FuzzyRuleAntecedent *terang_lembab_hangat = new FuzzyRuleAntecedent();
//  terang_lembab_hangat->joinWithAND(terang_lembab17, hangat17);
//
//  FuzzyRuleConsequent *cuaca17 = new FuzzyRuleConsequent();
//  cuaca17->addOutput(mendung);
//
//  FuzzyRule *fuzzyRule17 = new FuzzyRule(17,terang_lembab_hangat, cuaca17);
//  fuzzy->addFuzzyRule(fuzzyRule17);
//
//  //18 
//  FuzzyRuleAntecedent *terang_lembab18 = new FuzzyRuleAntecedent();
//  terang_lembab18->joinWithAND(terang, lembab);
//
//  FuzzyRuleAntecedent *panas18 = new FuzzyRuleAntecedent();
//  panas18->joinSingle(panas);
//
//  FuzzyRuleAntecedent *terang_lembab_panas = new FuzzyRuleAntecedent();
//  terang_lembab_panas->joinWithAND(terang_lembab18, panas18);
//
//  FuzzyRuleConsequent *cuaca18 = new FuzzyRuleConsequent();
//  cuaca18->addOutput(cerah);
//
//  FuzzyRule *fuzzyRule18 = new FuzzyRule(18,terang_lembab_panas, cuaca18);
//  fuzzy->addFuzzyRule(fuzzyRule18);
//
//  //19 
//  FuzzyRuleAntecedent *sangatTerang_kering19 = new FuzzyRuleAntecedent();
//  sangatTerang_kering19->joinWithAND(sangatTerang, kering);
//
//  FuzzyRuleAntecedent *dingin19 = new FuzzyRuleAntecedent();
//  dingin19->joinSingle(dingin);
//
//  FuzzyRuleAntecedent *sangatTerang_kering_dingin = new FuzzyRuleAntecedent();
//  sangatTerang_kering_dingin->joinWithAND(sangatTerang_kering19, dingin19);
//
//  FuzzyRuleConsequent *cuaca19 = new FuzzyRuleConsequent();
//  cuaca19->addOutput(cerah);
//
//  FuzzyRule *fuzzyRule19 = new FuzzyRule(19,sangatTerang_kering_dingin , cuaca19);
//  fuzzy->addFuzzyRule(fuzzyRule19);
//
//  //20  
//  FuzzyRuleAntecedent *sangatTerang_kering20 = new FuzzyRuleAntecedent();
//  sangatTerang_kering20->joinWithAND(sangatTerang, kering);
//
//  FuzzyRuleAntecedent *hangat20 = new FuzzyRuleAntecedent();
//  hangat20->joinSingle(hangat);
//
//  FuzzyRuleAntecedent *sangatTerang_kering_hangat = new FuzzyRuleAntecedent();
//  sangatTerang_kering_hangat->joinWithAND(sangatTerang_kering20, hangat20);
//
//  FuzzyRuleConsequent *cuaca20 = new FuzzyRuleConsequent();
//  cuaca20->addOutput(cerah);
//
//  FuzzyRule *fuzzyRule20 = new FuzzyRule(20,sangatTerang_kering_hangat , cuaca20);
//  fuzzy->addFuzzyRule(fuzzyRule20);
//
//  //21
//  FuzzyRuleAntecedent *sangatTerang_kering21 = new FuzzyRuleAntecedent();
//  sangatTerang_kering21->joinWithAND(sangatTerang, kering);
//
//  FuzzyRuleAntecedent *panas21 = new FuzzyRuleAntecedent();
//  panas21->joinSingle(panas);
//
//  FuzzyRuleAntecedent *sangatTerang_kering_panas = new FuzzyRuleAntecedent();
//  sangatTerang_kering_panas->joinWithAND(sangatTerang_kering21, panas21);
//
//  FuzzyRuleConsequent *cuaca21 = new FuzzyRuleConsequent();
//  cuaca21->addOutput(cerah);
//
//  FuzzyRule *fuzzyRule21 = new FuzzyRule(21,sangatTerang_kering_panas, cuaca21);
//  fuzzy->addFuzzyRule(fuzzyRule21);
//
//  //22
//  FuzzyRuleAntecedent *sangatTerang_normal22 = new FuzzyRuleAntecedent();
//  sangatTerang_normal22->joinWithAND(sangatTerang, normal);
//
//  FuzzyRuleAntecedent *dingin22 = new FuzzyRuleAntecedent();
//  dingin22->joinSingle(dingin);
//
//  FuzzyRuleAntecedent *sangatTerang_normal_dingin = new FuzzyRuleAntecedent();
//  sangatTerang_normal_dingin->joinWithAND(sangatTerang_normal22, dingin22);
//
//  FuzzyRuleConsequent *cuaca22 = new FuzzyRuleConsequent();
//  cuaca22->addOutput(cerah);
//
//  FuzzyRule *fuzzyRule22 = new FuzzyRule(22,sangatTerang_normal_dingin , cuaca22);
//  fuzzy->addFuzzyRule(fuzzyRule22);
//
//  //23
//  FuzzyRuleAntecedent *sangatTerang_normal23 = new FuzzyRuleAntecedent();
//  sangatTerang_normal23->joinWithAND(sangatTerang, normal);
//
//  FuzzyRuleAntecedent *hangat23= new FuzzyRuleAntecedent();
//  hangat23->joinSingle(hangat);
//
//  FuzzyRuleAntecedent *sangatTerang_normal_hangat = new FuzzyRuleAntecedent();
//  sangatTerang_normal_hangat->joinWithAND(sangatTerang_normal23, hangat23);
//
//  FuzzyRuleConsequent *cuaca23 = new FuzzyRuleConsequent();
//  cuaca23->addOutput(cerah);
//
//  FuzzyRule *fuzzyRule23 = new FuzzyRule(23,sangatTerang_normal_hangat, cuaca23);
//  fuzzy->addFuzzyRule(fuzzyRule23);
//
//  //24
//  FuzzyRuleAntecedent *sangatTerang_normal24 = new FuzzyRuleAntecedent();
//  sangatTerang_normal24->joinWithAND(sangatTerang, normal);
//
//  FuzzyRuleAntecedent *panas24= new FuzzyRuleAntecedent();
//  panas24->joinSingle(panas);
//
//  FuzzyRuleAntecedent *sangatTerang_normal_panas = new FuzzyRuleAntecedent();
//  sangatTerang_normal_panas->joinWithAND(sangatTerang_normal24, panas24);
//
//  FuzzyRuleConsequent *cuaca24 = new FuzzyRuleConsequent();
//  cuaca24->addOutput(cerah);
//
//  FuzzyRule *fuzzyRule24 = new FuzzyRule(24,sangatTerang_normal_panas, cuaca24);
//  fuzzy->addFuzzyRule(fuzzyRule24);
//
//
//  //25 
//  FuzzyRuleAntecedent *sangatTerang_lembab25 = new FuzzyRuleAntecedent();
//  sangatTerang_lembab25->joinWithAND(sangatTerang, lembab);
//
//  FuzzyRuleAntecedent *dingin25= new FuzzyRuleAntecedent();
//  dingin25->joinSingle(dingin);
//
//  FuzzyRuleAntecedent *sangatTerang_lembab_dingin = new FuzzyRuleAntecedent();
//  sangatTerang_lembab_dingin->joinWithAND(sangatTerang_lembab25, dingin25);
//
//  FuzzyRuleConsequent *cuaca25 = new FuzzyRuleConsequent();
//  cuaca25->addOutput(mendung);
//
//  FuzzyRule *fuzzyRule25 = new FuzzyRule(25,sangatTerang_lembab_dingin, cuaca25);
//  fuzzy->addFuzzyRule(fuzzyRule25);
//
//  //26 
//  FuzzyRuleAntecedent *sangatTerang_lembab26 = new FuzzyRuleAntecedent();
//  sangatTerang_lembab26->joinWithAND(sangatTerang, lembab);
//
//  FuzzyRuleAntecedent *hangat26= new FuzzyRuleAntecedent();
//  hangat26->joinSingle(hangat);
//
//  FuzzyRuleAntecedent *sangatTerang_lembab_hangat = new FuzzyRuleAntecedent();
//  sangatTerang_lembab_hangat->joinWithAND(sangatTerang_lembab26, hangat26);
//
//  FuzzyRuleConsequent *cuaca26 = new FuzzyRuleConsequent();
//  cuaca26->addOutput(cerah);
//
//  FuzzyRule *fuzzyRule26 = new FuzzyRule(26,sangatTerang_lembab_hangat, cuaca26);
//  fuzzy->addFuzzyRule(fuzzyRule26);
//
//  //27
//  FuzzyRuleAntecedent *sangatTerang_lembab27 = new FuzzyRuleAntecedent();
//  sangatTerang_lembab27->joinWithAND(sangatTerang, lembab);
//
//  FuzzyRuleAntecedent *panas27= new FuzzyRuleAntecedent();
//  panas27->joinSingle(panas);
//
//  FuzzyRuleAntecedent *sangatTerang_lembab_panas = new FuzzyRuleAntecedent();
//  sangatTerang_lembab_panas->joinWithAND(sangatTerang_lembab27, panas27);
//
//  FuzzyRuleConsequent *cuaca27 = new FuzzyRuleConsequent();
//  cuaca27->addOutput(cerah);
//
//  FuzzyRule *fuzzyRule27 = new FuzzyRule(27,sangatTerang_lembab_panas , cuaca27);
//  fuzzy->addFuzzyRule(fuzzyRule27);
}

void loop()
{
//  // get random entrances
    //masukkan cahaya 
    int input1 = random(30, 80);
    //masukkan kelembaban
    int input2 = random(50, 90);
    //masukkan suhu
    int input3 = random(0, 25);
    


//  //input cuaca
//  int input1 = analogRead(pinSensor);
//  //Serial.println(input1);
//
//  //input cahaya
//  int nilaiSensor = analogRead(LDR_Pin);
//  Serial.println(nilaiSensor);
//  //delay(250); //delay untuk memudahkan pembacaan
//  float input2 = (500.0/1024)*nilaiSensor;   

  Serial.println("");
  Serial.print("\n\n\nEntrance: ");
  Serial.print("\t\t\tcahaya: ");
  Serial.print(input1);
  Serial.print(", kelembaban: ");
  Serial.print(input2);
  Serial.print(", Suhu : ");
  Serial.print(input3);
  
  fuzzy->setInput(1, input1);
  fuzzy->setInput(2, input2);
  fuzzy->setInput(3, input3);

  fuzzy->fuzzify();

  Serial.println("Input: ");
  Serial.print("\thujan : redup-> ");
  Serial.print(redup->getPertinence());
  Serial.print(", terang-> ");
  Serial.print(terang->getPertinence());
  Serial.print(", Sangat Terang -> ");
  Serial.println(sangatTerang->getPertinence());

  Serial.print("\tgelap: kering -> ");
  Serial.print(kering->getPertinence());
  Serial.print(",  normal-> ");
  Serial.print(normal->getPertinence());
  Serial.print(",  lembab-> ");
  Serial.print(lembab->getPertinence());

  Serial.print("\tTemperature: dingin-> ");
  Serial.print(dingin->getPertinence());
  Serial.print(", Hangat -> ");
  Serial.print(hangat->getPertinence());
  Serial.print(", Panas-> ");
  Serial.println(panas->getPertinence());

  float output1 = fuzzy->defuzzify(1);
  // float output2 = fuzzy->defuzzify(2);

  Serial.println("Output: ");
  Serial.print("\tCuaca : -> Mendung ");
  Serial.print(mendung->getPertinence());
  Serial.print(", Cuaca-> ");
  Serial.print(cerah->getPertinence());

  Serial.println("Result: ");
  Serial.print("\t\t\t Cuaca: ");
  Serial.print(output1);

  // wait 12 seconds
  delay(5000);
}
