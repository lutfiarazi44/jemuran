#include <Fuzzy.h>

// For scope, instantiate all objects you will need to access in loop()
// It may be just one Fuzzy, but for demonstration, this sample will print
// all FuzzySet pertinence

// Fuzzy
Fuzzy *fuzzy = new Fuzzy();

// FuzzyInput cahaya
FuzzySet *redup = new FuzzySet(0, 0, 15, 50);
FuzzySet *terang = new FuzzySet(30, 50, 70, 80);
FuzzySet *sangatTerang = new FuzzySet(70, 80, 100, 100);

// FuzzyInput kelembaban
FuzzySet *kering = new FuzzySet(0, 0, 45, 60);
FuzzySet *normal = new FuzzySet(50, 60, 70, 90);
FuzzySet *lembab = new FuzzySet(80, 90, 100, 100);

// FuzzyOutput cuaca
FuzzySet *mendung = new FuzzySet(0, 0, 2, 6);
FuzzySet *cerah = new FuzzySet(4, 8, 10, 10);

//int LDR_Pin = A0; // pin a1 analog
//int pinSensor = A1;
void setup()
{
  // Set the Serial output
  Serial.begin(9600);
  // Set a random seed
  randomSeed(analogRead(0));

  // Every setup must occur in the function setup()

  // FuzzyInput
  FuzzyInput *cahaya = new FuzzyInput(1);

  cahaya->addFuzzySet(redup);
  cahaya->addFuzzySet(terang);
  cahaya->addFuzzySet(sangatTerang);
  fuzzy->addFuzzyInput(cahaya);

  // FuzzyInput
  FuzzyInput *kelembaban = new FuzzyInput(2);

  kelembaban->addFuzzySet(kering);
  kelembaban->addFuzzySet(normal);
  kelembaban->addFuzzySet(lembab);
  fuzzy->addFuzzyInput(kelembaban);

  // FuzzyOutput
  FuzzyOutput *cuaca = new FuzzyOutput(1);

  cuaca->addFuzzySet(mendung);
  cuaca->addFuzzySet(cerah);
  fuzzy->addFuzzyOutput(cuaca);

  // Building FuzzyRule 1
  FuzzyRuleAntecedent *redup_kering = new FuzzyRuleAntecedent();
  redup_kering->joinWithAND(redup, kering);
  
  FuzzyRuleConsequent *cuaca1 = new FuzzyRuleConsequent();
  cuaca1->addOutput(mendung);

  FuzzyRule *fuzzyRule1 = new FuzzyRule(1, redup_kering, cuaca1);
  fuzzy->addFuzzyRule(fuzzyRule1);

  // 2
  FuzzyRuleAntecedent *redup_normal = new FuzzyRuleAntecedent();
  redup_normal->joinWithAND(redup, normal);

  FuzzyRuleConsequent *cuaca2 = new FuzzyRuleConsequent();
  cuaca2->addOutput(mendung);

  FuzzyRule *fuzzyRule2 = new FuzzyRule(2, redup_normal, cuaca2);
  fuzzy->addFuzzyRule(fuzzyRule2);

  //3
  FuzzyRuleAntecedent *redup_lembab = new FuzzyRuleAntecedent();
  redup_lembab->joinWithAND(redup, lembab);

  FuzzyRuleConsequent *cuaca3 = new FuzzyRuleConsequent();
  cuaca3->addOutput(mendung);

  FuzzyRule *fuzzyRule3 = new FuzzyRule(3, redup_lembab, cuaca3);
  fuzzy->addFuzzyRule(fuzzyRule3);

  //4
  FuzzyRuleAntecedent *terang_kering = new FuzzyRuleAntecedent();
  terang_kering->joinWithAND(terang, kering);

  FuzzyRuleConsequent *cuaca4 = new FuzzyRuleConsequent();
  cuaca4->addOutput(cerah);

  FuzzyRule *fuzzyRule4 = new FuzzyRule(4, terang_kering, cuaca4);
  fuzzy->addFuzzyRule(fuzzyRule4);

  //5
  FuzzyRuleAntecedent *terang_normal = new FuzzyRuleAntecedent();
  terang_normal->joinWithAND(terang, normal);

  FuzzyRuleConsequent *cuaca5 = new FuzzyRuleConsequent();
  cuaca5->addOutput(cerah);

  FuzzyRule *fuzzyRule5 = new FuzzyRule(5, terang_normal, cuaca5);
  fuzzy->addFuzzyRule(fuzzyRule5);

  //6
  FuzzyRuleAntecedent *terang_lembab = new FuzzyRuleAntecedent();
  terang_lembab->joinWithAND(terang, lembab);

  FuzzyRuleConsequent *cuaca6 = new FuzzyRuleConsequent();
  cuaca6->addOutput(cerah);

  FuzzyRule *fuzzyRule6 = new FuzzyRule(6, terang_lembab, cuaca6);
  fuzzy->addFuzzyRule(fuzzyRule6);

  // 7 
  FuzzyRuleAntecedent *sangatTerang_kering = new FuzzyRuleAntecedent();
  sangatTerang_kering->joinWithAND(sangatTerang, kering);

  FuzzyRuleConsequent *cuaca7 = new FuzzyRuleConsequent();
  cuaca7->addOutput(cerah);

  FuzzyRule *fuzzyRule7 = new FuzzyRule(7, sangatTerang_kering, cuaca7);
  fuzzy->addFuzzyRule(fuzzyRule7);

  //8 
  FuzzyRuleAntecedent *sangatTerang_normal = new FuzzyRuleAntecedent();
  sangatTerang_normal->joinWithAND(sangatTerang, normal);

  FuzzyRuleConsequent *cuaca8 = new FuzzyRuleConsequent();
  cuaca8->addOutput(cerah);

  FuzzyRule *fuzzyRule8 = new FuzzyRule(8, sangatTerang_normal, cuaca8);
  fuzzy->addFuzzyRule(fuzzyRule8);

  // 9 
  FuzzyRuleAntecedent *sangatTerang_lembab = new FuzzyRuleAntecedent();
  sangatTerang_lembab->joinWithAND(sangatTerang, lembab);

  FuzzyRuleConsequent *cuaca9 = new FuzzyRuleConsequent();
  cuaca9->addOutput(cerah);

  FuzzyRule *fuzzyRule9 = new FuzzyRule(9, sangatTerang_lembab, cuaca9);
  fuzzy->addFuzzyRule(fuzzyRule9);
}

void loop()
{
//  // get random entrances
    //masukkan cahaya 
    int input1 = random(0, 80);
    //masukkan kelembaban
    int input2 = random(0, 90);

  Serial.println("");
  Serial.print("\n\n\nEntrance: ");
  Serial.print("\t\t\tcahaya: ");
  Serial.print(input1);
  Serial.print(", kelembaban: ");
  Serial.print(input2);
  
  fuzzy->setInput(1, input1);
  fuzzy->setInput(2, input2);

  fuzzy->fuzzify();

  Serial.println("Input: ");
  Serial.print("\thujan : redup-> ");
  Serial.print(redup->getPertinence());
  Serial.print(", terang-> ");
  Serial.print(terang->getPertinence());
  Serial.print(", Sangat Terang -> ");
  Serial.println(sangatTerang->getPertinence());

  Serial.print("\tgelap: kering -> ");
  Serial.print(kering->getPertinence());
  Serial.print(",  normal-> ");
  Serial.print(normal->getPertinence());
  Serial.print(",  lembab-> ");
  Serial.print(lembab->getPertinence());

  float output1 = fuzzy->defuzzify(1);

  Serial.println("Output: ");
  Serial.print("\tCuaca : -> Mendung ");
  Serial.print(mendung->getPertinence());
  Serial.print(", Cuaca-> ");
  Serial.print(cerah->getPertinence());

  Serial.println("Result: ");
  Serial.print("\t\t\t Cuaca: ");
  Serial.print(output1);

  // wait 12 seconds
  delay(5000);
}
