#include "DHT.h"
#include <Fuzzy.h>
#include <Stepper.h>

#define IN1   8   // IN1 is connected to NodeMCU pin D1 (GPIO5)
#define IN2   9   // IN2 is connected to NodeMCU pin D2 (GPIO4)
#define IN3   10   // IN3 is connected to NodeMCU pin D3 (GPIO0)
#define IN4   11   // IN4 is connected to NodeMCU pin D4 (GPIO2)
#define LED   13
#define DHTPIN 4
#define DHTTYPE DHT11

DHT dht(DHTPIN, DHTTYPE);

int tandaJemuranMasuk=0;
int tandaJemuranKeluar=0;

const int stepsPerRevolution = 2048; 
Stepper myStepper(stepsPerRevolution, IN1, IN3, IN2, IN4);
//int stepCount = 0;

// For scope, instantiate all objects you will need to access in loop()
// It may be just one Fuzzy, but for demonstration, this sample will print
// all FuzzySet pertinence
// global inisialisasi 

float LDR_Pin = A0; // pin 0 analog

// Fuzzy
Fuzzy *fuzzy = new Fuzzy();

// FuzzyInput cahaya
FuzzySet *redup = new FuzzySet(0, 0, 15, 50);
FuzzySet *terang = new FuzzySet(30, 50, 70, 80);
FuzzySet *sangatTerang = new FuzzySet(70, 80, 100, 100);

// FuzzyInput kelembaban
FuzzySet *kering = new FuzzySet(0, 0, 45, 60);
FuzzySet *normal = new FuzzySet(50, 60, 70, 90);
FuzzySet *lembab = new FuzzySet(80, 90, 100, 100);

// FuzzyOutput cuaca
FuzzySet *mendung = new FuzzySet(0, 0, 2, 6);
FuzzySet *cerah = new FuzzySet(4, 8, 10, 10);

//int LDR_Pin = A0; // pin a1 analog
//int pinSensor = A1;

void setup()
{ 
  pinMode(LED, OUTPUT);
  // Set the Serial output
  Serial.begin(9600);
  
  //randomSeed(analogRead(0));
  dht.begin();

  fuzzySet();
  fuzzyRule();
  
}

void loop()
{
  myStepper.setSpeed(10);

  // masukkan cahaya   
  float nilaiSensor = analogRead(A1);
  delay(250); 
  float lux = (100.0/1024)*nilaiSensor;
  Serial.print("Cahaya :");
  Serial.println(lux);

  //masukkan kelembaban 
  float input2a = dht.readHumidity();
  Serial.print ("Kelembaban :");
  Serial.println(input2a);
  


  float input1 = lux;
  float input2 = input2a;

  int hujan = digitalRead(7);

  
  fuzzy->setInput(1, input1);
  fuzzy->setInput(2, input2);

  fuzzy->fuzzify();


  float output1 = fuzzy->defuzzify(1);
  float a = output1;


  Serial.println("");
  Serial.print("Result: ");
  Serial.print("\t\t\t Cuaca: ");
  Serial.print(a);

  //kondisi cuaca
  Serial.println(hujan);
  if (hujan == 0){
    jemuranMasuk();
  }

  else {  
    if (a>0 && a<=5) {
      jemuranMasuk();
      }
      
    else if (a>5 && a<=10) {
      jemuranKeluar();
      }
  }     
}

//////////////////////
void jemuranMasuk() {
    // min masuk
    if (tandaJemuranMasuk == 0) {
//      myStepper.setSpeed(kecepatan);
      myStepper.step(stepsPerRevolution);
      Serial.print("Jemuran Masuk");
      delay (2000);
      myStepper.setSpeed(0);
      digitalWrite(LED, LOW);
      tandaJemuranMasuk=1;
      tandaJemuranKeluar=0;
      
     }

  else {
    myStepper.setSpeed(0);
    tandaJemuranKeluar=0;
    Serial.println("Jemuran ditahan di dalam");
    }
}

void jemuranKeluar() {
    // min masuk
    if (tandaJemuranKeluar == 0) {
//      myStepper.setSpeed(kecepatan);
      myStepper.step(-stepsPerRevolution);
      Serial.print("Jemuran Keluar");  
      delay (2000);
      myStepper.setSpeed(0);
      digitalWrite(LED, HIGH);
      tandaJemuranKeluar=1;
      tandaJemuranMasuk=0; 
   }

  else {
    myStepper.setSpeed(0);
    tandaJemuranMasuk=0;
    Serial.println("Jemuran ditahan diluar");
    }
}



void fuzzySet(){
  // FuzzyInput
  FuzzyInput *cahaya = new FuzzyInput(1);

  cahaya->addFuzzySet(redup);
  cahaya->addFuzzySet(terang);
  cahaya->addFuzzySet(sangatTerang);
  fuzzy->addFuzzyInput(cahaya);

  // FuzzyInput
  FuzzyInput *kelembaban = new FuzzyInput(2);

  kelembaban->addFuzzySet(kering);
  kelembaban->addFuzzySet(normal);
  kelembaban->addFuzzySet(lembab);
  fuzzy->addFuzzyInput(kelembaban);

  // FuzzyOutput
  FuzzyOutput *cuaca = new FuzzyOutput(1);

  cuaca->addFuzzySet(mendung);
  cuaca->addFuzzySet(cerah);
  fuzzy->addFuzzyOutput(cuaca);
  }

void fuzzyRule(){
  // Building FuzzyRule 1
  FuzzyRuleAntecedent *redup_kering = new FuzzyRuleAntecedent();
  redup_kering->joinWithAND(redup, kering);
  
  FuzzyRuleConsequent *cuaca1 = new FuzzyRuleConsequent();
  cuaca1->addOutput(mendung);

  FuzzyRule *fuzzyRule1 = new FuzzyRule(1, redup_kering, cuaca1);
  fuzzy->addFuzzyRule(fuzzyRule1);

  // 2
  FuzzyRuleAntecedent *redup_normal = new FuzzyRuleAntecedent();
  redup_normal->joinWithAND(redup, normal);

  FuzzyRuleConsequent *cuaca2 = new FuzzyRuleConsequent();
  cuaca2->addOutput(mendung);

  FuzzyRule *fuzzyRule2 = new FuzzyRule(2, redup_normal, cuaca2);
  fuzzy->addFuzzyRule(fuzzyRule2);

  //3
  FuzzyRuleAntecedent *redup_lembab = new FuzzyRuleAntecedent();
  redup_lembab->joinWithAND(redup, lembab);

  FuzzyRuleConsequent *cuaca3 = new FuzzyRuleConsequent();
  cuaca3->addOutput(mendung);

  FuzzyRule *fuzzyRule3 = new FuzzyRule(3, redup_lembab, cuaca3);
  fuzzy->addFuzzyRule(fuzzyRule3);

  //4
  FuzzyRuleAntecedent *terang_kering = new FuzzyRuleAntecedent();
  terang_kering->joinWithAND(terang, kering);

  FuzzyRuleConsequent *cuaca4 = new FuzzyRuleConsequent();
  cuaca4->addOutput(cerah);

  FuzzyRule *fuzzyRule4 = new FuzzyRule(4, terang_kering, cuaca4);
  fuzzy->addFuzzyRule(fuzzyRule4);

  //5
  FuzzyRuleAntecedent *terang_normal = new FuzzyRuleAntecedent();
  terang_normal->joinWithAND(terang, normal);

  FuzzyRuleConsequent *cuaca5 = new FuzzyRuleConsequent();
  cuaca5->addOutput(cerah);

  FuzzyRule *fuzzyRule5 = new FuzzyRule(5, terang_normal, cuaca5);
  fuzzy->addFuzzyRule(fuzzyRule5);

  //6
  FuzzyRuleAntecedent *terang_lembab = new FuzzyRuleAntecedent();
  terang_lembab->joinWithAND(terang, lembab);

  FuzzyRuleConsequent *cuaca6 = new FuzzyRuleConsequent();
  cuaca6->addOutput(cerah);

  FuzzyRule *fuzzyRule6 = new FuzzyRule(6, terang_lembab, cuaca6);
  fuzzy->addFuzzyRule(fuzzyRule6);

  // 7 
  FuzzyRuleAntecedent *sangatTerang_kering = new FuzzyRuleAntecedent();
  sangatTerang_kering->joinWithAND(sangatTerang, kering);

  FuzzyRuleConsequent *cuaca7 = new FuzzyRuleConsequent();
  cuaca7->addOutput(cerah);

  FuzzyRule *fuzzyRule7 = new FuzzyRule(7, sangatTerang_kering, cuaca7);
  fuzzy->addFuzzyRule(fuzzyRule7);

  //8 
  FuzzyRuleAntecedent *sangatTerang_normal = new FuzzyRuleAntecedent();
  sangatTerang_normal->joinWithAND(sangatTerang, normal);

  FuzzyRuleConsequent *cuaca8 = new FuzzyRuleConsequent();
  cuaca8->addOutput(cerah);

  FuzzyRule *fuzzyRule8 = new FuzzyRule(8, sangatTerang_normal, cuaca8);
  fuzzy->addFuzzyRule(fuzzyRule8);

  // 9 
  FuzzyRuleAntecedent *sangatTerang_lembab = new FuzzyRuleAntecedent();
  sangatTerang_lembab->joinWithAND(sangatTerang, lembab);

  FuzzyRuleConsequent *cuaca9 = new FuzzyRuleConsequent();
  cuaca9->addOutput(cerah);

  FuzzyRule *fuzzyRule9 = new FuzzyRule(9, sangatTerang_lembab, cuaca9);
  fuzzy->addFuzzyRule(fuzzyRule9);
  }