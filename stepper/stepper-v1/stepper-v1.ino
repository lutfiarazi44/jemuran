
/*
 Stepper Motor Control - speed control

 This program drives a unipolar or bipolar stepper motor.
 The motor is attached to digital pins 8 - 11 of the Arduino.
 A potentiometer is connected to analog input 0.

 The motor will rotate in a clockwise direction. The higher the potentiometer value,
 the faster the motor speed. Because setSpeed() sets the delay between steps,
 you may notice the motor is less responsive to changes in the sensor value at
 low speeds.

 Created 30 Nov. 2009
 Modified 28 Oct 2010
 by Tom Igoe

 */

#include <Stepper.h>
#define IN1   8   // IN1 is connected to NodeMCU pin D1 (GPIO5)
#define IN2   9   // IN2 is connected to NodeMCU pin D2 (GPIO4)
#define IN3   10   // IN3 is connected to NodeMCU pin D3 (GPIO0)
#define IN4   11   // IN4 is connected to NodeMCU pin D4 (GPIO2)

int kecepatan = -100 ;
int kecepatan1;
const int stepsPerRevolution = 200;  // change this to fit the number of steps per revolution
// for your motor


// initialize the stepper library on pins 8 through 11:
Stepper myStepper(stepsPerRevolution, IN4, IN2, IN3, IN1);

int stepCount = 0;  // number of steps the motor has taken

void setup() {
  Serial.begin(9600);
  // nothing to do inside the setup
}

void loop() {
  
  // nilai fuzzy kecepatan 
    
  if (kecepatan >= 0) {
    myStepper.setSpeed(kecepatan);
    // step 1/100 of a revolution:
    myStepper.step(stepsPerRevolution / 100);

  
  
  }

  else if (kecepatan <=0) {
    kecepatan1 = kecepatan*-1;
    myStepper.setSpeed(kecepatan1);
    myStepper.step(-stepsPerRevolution / 100);

//    
  }

}
