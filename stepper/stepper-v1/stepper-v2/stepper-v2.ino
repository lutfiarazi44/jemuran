#include <Arduino_FreeRTOS.h>
#include <Stepper.h>


#define IN1   8   // IN1 is connected to NodeMCU pin D1 (GPIO5)
#define IN2   9   // IN2 is connected to NodeMCU pin D2 (GPIO4)
#define IN3   10   // IN3 is connected to NodeMCU pin D3 (GPIO0)
#define IN4   11   // IN4 is connected to NodeMCU pin D4 (GPIO2)

int kecepatan = -100 ;
int kecepatan1;
int cuaca = 1023;
int cahaya= 500 ;
const int stepsPerRevolution = 200;
Stepper myStepper(stepsPerRevolution, IN4, IN2, IN3, IN1);
int stepCount = 0;

// define two tasks for Blink & AnalogRead
void TaskMasuk( void *pvParameters );
void TaskJemur( void *pvParameters );

// the setup function runs once when you press reset or power the board
void setup() {
  
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB, on LEONARDO, MICRO, YUN, and other 32u4 based boards.
  }

  // Now set up two tasks to run independently.
  xTaskCreate(
    TaskMasuk
    ,  "Blink"   // A name just for humans
    ,  128  // This stack size can be checked & adjusted by reading the Stack Highwater
    ,  NULL
    ,  2  // Priority, with 3 (configMAX_PRIORITIES - 1) being the highest, and 0 being the lowest.
    ,  NULL );

  xTaskCreate(
    TaskJemur
    ,  "AnalogRead"
    ,  128  // Stack size
    ,  NULL
    ,  1  // Priority
    ,  NULL );

  // Now the task scheduler, which takes over control of scheduling individual tasks, is automatically started.
}

void loop()
{
  // Empty. Things are done in Tasks.
}

/*--------------------------------------------------*/
/*---------------------- Tasks ---------------------*/
/*--------------------------------------------------*/

void Jemur (void *pvParameters)  // This is a task.
{
  (void) pvParameters;

  // initialize digital LED_BUILTIN on pin 13 as an output.
  pinMode(LED_BUILTIN, OUTPUT);

  int i = 0 ;
  if (cuaca <=1023 && cahaya <=500 ){
  for (i; i<=5;i++) // A Task shall never return or exit.
  {
    kecepatan1 = kecepatan*-1;
    myStepper.setSpeed(kecepatan1);
    myStepper.step(-stepsPerRevolution / 100);
  }
  
  }
}

void TaskMasuk(void *pvParameters)  // This is a task.
{
  (void) pvParameters;
  
/*
  AnalogReadSerial
  Reads an analog input on pin 0, prints the result to the serial monitor.
  Graphical representation is available using serial plotter (Tools > Serial Plotter menu)
  Attach the center pin of a potentiometer to pin A0, and the outside pins to +5V and ground.

  This example code is in the public domain.
*/

  for (;;)
  {
    // read the input on analog pin 0:
    int sensorValue = analogRead(A0);
    // print out the value you read:
    Serial.println(sensorValue);
    vTaskDelay(1);  // one tick delay (15ms) in between reads for stability
  }
}
