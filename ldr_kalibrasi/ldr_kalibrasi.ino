int LDR_Pin = A0; // pin 0 analog

void setup(){
  Serial.begin(9600);
}

void loop(){
  int nilaiSensor = analogRead(A1); 
  Serial.println(nilaiSensor);
  delay(250); //delay untuk memudahkan pembacaan
  float lux = (100.0/1024)*nilaiSensor;
  Serial.print("Lux:");
  Serial.println(lux);
}
